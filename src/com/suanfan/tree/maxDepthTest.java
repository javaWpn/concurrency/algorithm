package com.suanfan.tree;

public class maxDepthTest {
    public static void main(String[] args) {
        int[] arr = {8, 6, 10, 5, 7, 9, 11};
        TreeNode root = TreeNode.creatBinTree(arr, 0);
        System.out.println(maxDepth(root));
        System.out.println(hasPathSum(root,21));
    }

    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }

    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        sum -= root.data;
        if (sum == 0 && root.left == null && root.right == null) {
            return true;
        }else {
            return hasPathSum(root.left,sum) || hasPathSum(root.right,sum);
        }
    }

}
