package com.suanfan.tree;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.*;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;

public class TreeNode {
    int data;
    TreeNode left;
    TreeNode right;

    public TreeNode(int num) {
        left = null;
        right = null;
        data = num;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", left=" + left +
                ", right=" + right +
                '}';
    }

    /**
     * 递归按顺序构建二叉树
     *
     * @param arr
     * @param num
     * @return
     */
    public static TreeNode creatBinTree(int[] arr, int num) {
        TreeNode root = new TreeNode(arr[num]);
        if (2 * num + 1 < arr.length) {
            root.left = creatBinTree(arr, 2 * num + 1);
        }
        if (2 * num + 2 < arr.length) {
            root.right = creatBinTree(arr, 2 * num + 2);
        }
        return root;
    }

    /**
     * 数先序遍历（根左右）
     *
     * @param root
     */
    public static void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.println(root.data);
        preOrder(root.left);
        preOrder(root.right);
    }

    public static int[] preOrders(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        ArrayList<Integer> list = new ArrayList<>();
        preOrderList(root, list);
        return list.stream().mapToInt(Integer::intValue).toArray();
    }

    public static void preOrderList(TreeNode root, ArrayList list) {
        if (root != null) {
            list.add(root.data);
            preOrderList(root.left, list);
            preOrderList(root.right, list);
        }
    }

    /**
     * 数中序遍历（左根右）
     *
     * @param root
     */
    public static void midOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        midOrder(root.left);
        System.out.println(root.data);
        midOrder(root.right);
    }

    /**
     * 数后序遍历（左右根）
     *
     * @param root
     */
    public static void nextOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        nextOrder(root.left);
        nextOrder(root.right);
        System.out.println(root.data);
    }

    /**
     * 层次遍历(广度优先遍历)
     *
     * @param Node
     */
    public static void levelOrder(TreeNode Node) {
        if (Node == null) {
            return;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(Node);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            System.out.println(node.data);
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }
    }

    /**
     * 树利用栈先序遍历（根左右）（深度优先搜索）
     *
     * @param root
     */
    public static void preStackOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            System.out.println(node.data);
            if (node.right != null) {
                stack.push(node.right);
            }
            if (node.left != null) {
                stack.push(node.left);
            }
        }
    }

    /**
     * 树利用栈中序遍历（左根右）
     * 1，首先从根节点出发一路向左，入栈所有的左节点；
     * 2，出栈一个节点，输出该节点val值，查询该节点是否存在右节点，
     * 若存在则从该右节点出发一路向左入栈该右节点所在子树所有的左节点；
     * 3，若不存在右节点，则出栈下一个节点，输出节点val值，同步骤2操作；
     * 4，直到节点为null，且栈为空。
     *
     * @param root
     */
    public static void midStackOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            if (!stack.isEmpty()) {
                TreeNode node = stack.pop();
                System.out.println(node.data);
                root = node.right;
            }
        }
    }

    /**
     * 树利用栈后序遍历（左右根）
     *
     * @param root
     */
    public static void nextStackOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        TreeNode left = root.left;
        while (left != null) {
            stack.push(left);
            left = left.left;
        }
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            System.out.println(node.data);
            if (node.right != null) {
                TreeNode right = node.right;
                stack.push(right);
                TreeNode left1 = right.left;
                while (left1 != null) {
                    stack.push(left1);
                    left1 = left1.left;
                }
                node = stack.pop();
            }
        }
    }

    private static List res = new ArrayList();

    public static List binaryTreePaths(TreeNode root) {
        binaryTreePathStr(root, new String());
        return res;
    }

    private static void binaryTreePathStr(TreeNode root, String str) {
        if (root == null) {
            return;
        }
        //找到每一路的最后一个
        if (root.left == null && root.right == null) {
            str += root.data;
            res.add(str);
            return;
        } else {
            binaryTreePathStr(root.left, str + root.data + "->");
            binaryTreePathStr(root.right, str + root.data + "->");
        }
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        TreeNode root = creatBinTree(arr, 0);
//        System.out.println(root);
//        preOrder(root);
        System.out.println(binaryTreePaths(root));
//        System.out.println( Arrays.toString(preOrders(root)));
//        System.out.println(Arrays.toString(arr));
//        System.out.println("--------------------------------");
//        midOrder(root);
//        System.out.println("--------------------------------");
//        nextOrder(root);
//        System.out.println("--------------------------------");
//        levelOrder(root);
//        System.out.println("--------------------------------");
//        preStackOrder(root);
//        System.out.println("--------------------------------");
//        midStackOrder(root);
//        System.out.println("--------------------------------");
//        nextStackOrder(root);
    }
}
