package com.suanfan.tree;

import java.util.*;

public class PrintTest {
    public static void main(String[] args) {
        int[] arr = {8, 6, 10, 5, 7, 9, 11};
        TreeNode root = TreeNode.creatBinTree(arr, 0);
        System.out.println(Print(root));
    }

    public static ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        if (pRoot == null) return list;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(pRoot);
        boolean flag = true;
        while (!queue.isEmpty()) {
            int n = queue.size();
            flag = !flag;
            ArrayList<Integer> row = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                TreeNode node = queue.poll();
                row.add(node.data);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            if (flag){
                Collections.reverse(row);
            }
            list.add(row);
        }
        return list;
    }
}
