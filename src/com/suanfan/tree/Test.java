package com.suanfan.tree;

import java.util.*;

public class Test {
    public static ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
        if (root == null){
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            ArrayList<Integer> integers = new ArrayList<>();
            for (int size = queue.size(); size > 0; size--) {
                TreeNode node = queue.poll();
                integers.add(node.data);
                if (node.left != null){
                    queue.add(node.left);
                }
                if (node.right != null){
                    queue.add(node.right);
                }
            }
            list.add(integers);
        }
        return list;
    }

    public static int search (int[] nums, int target) {
        if (nums.length > 0){
            int lower = 0;
            int higher = nums.length-1;
            while (lower <= higher){
                int middle = (higher+lower)/2;
                if (nums[middle] == target){
                    return middle;
                }
                if (nums[middle] > target){
                    higher = middle - 1;
                }
                if (nums[middle] < target){
                    lower = middle + 1;
                }
            }
        }
        return -1;
    }

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new LinkedList<>();
        inorderTraversalList(root,list);
        return list;
    }

    public static void inorderTraversalList(TreeNode root, List list) {
        inorderTraversalList(root,list);
        list.add(root.data);
        inorderTraversalList(root,list);
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
//        TreeNode root = TreeNode.creatBinTree(arr, 0);
//        System.out.println(levelOrder(root));
        System.out.println(search(arr, 4));
    }
}
