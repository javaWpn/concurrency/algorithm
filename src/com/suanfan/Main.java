package com.suanfan;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
//        int[] ints = twoSumHandel();
//        System.out.println(Arrays.toString(ints));
        long start = System.currentTimeMillis();
        boolean flag = isPalindromeHandel();
        long end = System.currentTimeMillis();
        System.out.println(end-start+"ms");
        System.out.println(flag);
    }

    //俩数之和
    public static int[] twoSumHandel(){
        int inputArr[] = {3,2,4};
        int target = 6;
        return twoSum2(inputArr, target);
    }

    //回文数
    public static boolean isPalindromeHandel(){
        int x = 121;
        return isPalindrome1(x);
    }

    //第一种
    public static int[] twoSum1(int[] nums, int target) {
        for(int i=0,len=nums.length;i<len;i++) {
            for (int j = i+1; j <= len-1; j++) {
                if (nums[i]+nums[j]==target){
                    for (int k = 0; k <len ; k++) {
                        if (i == nums[k] || j == nums[k])break;
                    }
                    int resultArr[] = {i,j};
                    return resultArr;
                }
            }
        }
        return  null;
    }

    //第二种
    public static int[] twoSum2(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i=0,len=nums.length;i<len;i++) {
            if (!map.containsKey((target-nums[i]))){
                map.put(nums[i],i );
            }else {
                int [] resultIndex = {map.get(target-nums[i]),i};
                return resultIndex;
            }
        }
        return  null;
    }

    //第一种
    public static boolean isPalindrome1(int x) {
        if (x < 0) return false;
        String xArr[] = String.valueOf(x).split("");
        boolean flag = false;
        int index = xArr.length % 2 == 0 ? xArr.length/2 : (int) (Math.ceil(xArr.length / 2));
        if (index == 0) return true;
        for (int i = 0;i < index ; i++) {
            if (xArr[i].equals(xArr[xArr.length-(i+1)])){
                flag = true;
            }else {
                return false;
            }
        }
        return flag;
    }

    //第二种
    public static boolean isPalindrome2(int x) {
        if (x<0) return false;
        String xArr[] = String.valueOf(x).split("");
        if (xArr.length == 0) return true;
        boolean flag = false;
        String fstr = "";
        for (int i = xArr.length-1; i >= 0 ; i--) {
            fstr += xArr[i];
        }
        try {
            if (Integer.parseInt(fstr) == x) flag = true;
        }catch (NumberFormatException e){
            flag = false;
        }
        return flag;
    }

    //第三种
    public static boolean isPalindrome3(int x) {
        String s = String.valueOf(x);
        return new StringBuffer(s).reverse().toString().equals(s);
    }


}
