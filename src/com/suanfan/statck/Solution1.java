package com.suanfan.statck;

import java.util.Stack;

public class Solution1 {
    Stack<Integer> stack = new Stack<Integer>();
    public void push(int node) {
        stack.push(node);
    }

    public void pop() {
        stack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int min() {
        int min = top();
        for (int i = 0; i < stack.size()-1; i++) {
            int node = stack.get(i);
            if (min > node){
                min = node;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        Solution1 solution1 = new Solution1();
        solution1.push(-1);
        solution1.push(2);
        System.out.println(solution1.min());
        System.out.println(solution1.top());
        solution1.pop();
        solution1.push(1);
        System.out.println(solution1.top());
        System.out.println(solution1.min());
    }
}
