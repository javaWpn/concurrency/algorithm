package com.suanfan.sort;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * @author Wpn
 * @title: TopK
 * @projectName 算法
 * @description: TODO
 * @date 2022/2/7 16:28
 */
public class TopK {
    public static void main(String[] args) {
        int arr[] = {3, 24, 35, 46, 13, 25, 2, 56, 1};
        System.out.println(topk(arr, 8));
    }

    private static TreeSet<Integer> topk(int[] array, int n) {
        TreeSet<Integer> set = new TreeSet<Integer>();
        for (int i = 0; i < array.length; i++) {

            int value = array[i];
            if (set.size() < n)
                set.add(value);
            else {
                Iterator<Integer> it = set.descendingIterator();
                int setMax = it.next();
                if (setMax > value) {
                    it.remove();
                    set.add(value);
                }
            }
        }
        return set;

    }
}
