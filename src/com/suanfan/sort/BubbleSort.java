package com.suanfan.sort;

import java.util.Arrays;

/**
 * @author Wpn
 * @title: BubbleSort:冒泡排序 稳定
 * @projectName 算法
 * @description: TODO
 * @date 2022/2/7 14:38
 */
public class BubbleSort {
    public static void main(String[] args) {
        int arr[] = {3,34,5,7,2,48,19,27};
        int len = arr.length;
        for (int i = 0; i < len-1; i++) {
            for (int j = 0; j < len-1-i; j++) {
                if (arr[j] > arr[j+1]){
                    int temp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
