package com.suanfan.sort;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

/**
 * @author Wpn
 * @title: InsertSort:插入排序 稳定
 * @projectName 算法
 * @description: TODO
 * @date 2022/2/7 15:26
 */
public class InsertSort {
    public static void main(String[] args) {
        int arr[] = {3,7,45,26,18,39,20,10};
        int len = arr.length;
        for (int i = 1; i < len; i++) {
            if (arr[i] < arr[i-1]){
                int temp = arr[i];
                int j = i-1;
                for (; j >=0 && arr[j] > temp ; j--) {
                    arr[j+1] = arr[j];
                }
                arr[j+1] = temp;
            }
        }
        System.out.println(Arrays.toString(arr));
    }

}
