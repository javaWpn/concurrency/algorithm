package com.suanfan.sort;

import java.util.Arrays;

/**
 * @author Wpn
 * @title: SelectionSort:选择排序 不稳定
 * @projectName 算法
 * @description: TODO
 * @date 2022/2/7 14:52
 */
public class SelectionSort {
    public static void main(String[] args) {
        int arr[] = {3, 24, 35, 46, 13, 25, 2, 56, 1};
        int len = arr.length;
        for (int i = 0; i < len - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < len; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                int temp = arr[minIndex];
                arr[minIndex] = arr[i];
                arr[i] = temp;
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
