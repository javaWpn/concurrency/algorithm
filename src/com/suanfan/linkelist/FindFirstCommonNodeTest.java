package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.HashSet;
import java.util.Set;

public class FindFirstCommonNodeTest {
    public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
        Set<ListNode> set = new HashSet<>();
        while (pHead1 != null) {
            set.add(pHead1);
            pHead1 = pHead1.next;
        }
        while (pHead2 != null) {
            if (!set.add(pHead2)) {
                return pHead2;
            }
            pHead2 = pHead2.next;
        }
        return pHead2;
    }
}
