package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.List;

/**
 * @author Wpn
 * @title: RangeReversal
 * @projectName 算法
 * @description: 区间单链表反转
 * @date 2022/6/24 14:41
 */
public class RangeReversal {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(4);
        final ListNode listNode5 = new ListNode(1);
        final ListNode listNode6 = new ListNode(4);
        final ListNode listNode7 = new ListNode(-1);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        long startTime = System.currentTimeMillis();
        System.out.println(RangeReversal(listNode1, 3, 6));
        System.out.println(System.currentTimeMillis()-startTime+"ms");
    }

    public static ListNode RangeReversal(ListNode head, int m, int n) {
        System.out.println(head);
        if (head == null || head.next == null || m == n) {
            return head;
        }
        ListNode pre = null;
        ListNode rangePre = null;
        ListNode next = null;
        int num = 0;
        while (head != null) {
            next = head.next;
            num++;
            if (num < m) {
                head.next = null;
                if (pre == null) {
                    pre = head;
                } else {
                    ListNode cur = pre;
                    while (cur.next != null) {
                        cur = cur.next;
                    }
                    cur.next = head;
                }
            }
            if (num >= m && num <= n) {
                head.next = rangePre;
                rangePre = head;
            }
            if (num > n) {
                ListNode cur = rangePre;
                while (cur.next != null) {
                    cur = cur.next;
                }
                cur.next = head;
                break;
            }
            head = next;
        }
        if (pre != null) {
            ListNode curs = pre;
            while (curs.next != null) {
                curs = curs.next;
            }
            curs.next = rangePre;
        } else {
            pre = rangePre;
        }
        return pre;
    }

    public static ListNode RangeReversalGood(ListNode head, int m, int n) {
        ListNode dummyNode = new ListNode(-1);
        dummyNode.next = head;
        ListNode pre = dummyNode;
        for (int i = 0; i < m - 1; i++) {
            pre = pre.next;
        }
        ListNode cur = pre.next;
        ListNode Cur_next;
        for (int i = 0; i < n - m; i++) {
            Cur_next = cur.next;
            cur.next = Cur_next.next;
            Cur_next.next = pre.next;
            pre.next = Cur_next;
        }
        return dummyNode.next;
    }
}
