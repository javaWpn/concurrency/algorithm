package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

/**
 * @author Wpn
 * @title: LinkListTest
 * @projectName 算法
 * @description: TODO
 * @date 2022/6/27 14:39
 */
public class LinkListTest {
    public static void main(String[] args) {

        ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        System.out.println(addLast(listNode1));
    }

    /**
     * @param head
     * @Description:头插法（链表反转）
     * @Author: wpn
     * @Date: 2022/6/27 14:50
     * @return: com.suanfan.linkelist.domain.ListNode
     **/
    public static ListNode addFirst(ListNode head) {
        ListNode pre = null;
        ListNode next = null;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    /**
     * @param head
     * @Description:尾插法
     * @Author: wpn
     * @Date: 2022/6/27 14:50
     * @return: com.suanfan.linkelist.domain.ListNode
     **/
    public static ListNode addLast(ListNode head) {
        ListNode pre = new ListNode(0);
        while (head != null) {
            ListNode newNode = head;
            if (pre == null) {
                pre = newNode;
                return pre;
            }
            ListNode cur = pre;
            while (cur.next != null) {
                cur = cur.next;
            }
            cur.next = newNode;
            head = head.next;
        }
        return pre;
    }
}
