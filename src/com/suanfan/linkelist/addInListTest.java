package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.Stack;

public class addInListTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(6);
        final ListNode listNode2 = new ListNode(3);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(9);
        final ListNode listNode5 = new ListNode(3);
        final ListNode listNode6 = new ListNode(7);
//        final ListNode listNode7 = new ListNode(1);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
//        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
//        listNode6.next = listNode7;
        System.out.println(addInList(listNode1, listNode4));
    }

    public static ListNode addInList(ListNode head1, ListNode head2) {
        Stack<ListNode> stack1 = new Stack<>();
        Stack<ListNode> stack2 = new Stack<>();
        while (head1 != null) {
            stack1.push(head1);
            head1 = head1.next;
        }
        while (head2 != null) {
            stack2.push(head2);
            head2 = head2.next;
        }
        ListNode newHead = null;
        ListNode head = null;
        int num1 = 0;
        while (!stack1.isEmpty() || !stack2.isEmpty() || num1 != 0){
            int x = stack1.isEmpty() ? 0 : stack1.pop().val;
            int y = stack2.isEmpty() ? 0 : stack2.pop().val;
            int sum = x + y + num1;
            num1 = sum / 10;
            newHead = new ListNode(sum % 10);
            newHead.next = head;
            head = newHead;
        }
        return head;
    }
}
