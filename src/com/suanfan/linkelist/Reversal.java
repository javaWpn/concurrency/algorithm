package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;
import sun.text.normalizer.UCharacter;

/**
 * @author Wpn
 * @title: Reversal
 * @projectName 链表反转
 * @description: TODO
 * @date 2022/6/24 14:13
 */
public class Reversal {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        System.out.println(Reversal(listNode1));
    }

    public static ListNode Reversal(ListNode head){
        System.out.println(head);
        if (head == null || head.next == null){
            return head;
        }
        ListNode pre = null;
        ListNode next = null;
        while (head != null){
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

}
