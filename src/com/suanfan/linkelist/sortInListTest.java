package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

public class sortInListTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(5);
        final ListNode listNode5 = new ListNode(1);
        final ListNode listNode6 = new ListNode(4);
        final ListNode listNode7 = new ListNode(-1);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        System.out.println(sortInList(listNode1));
    }

    //虚假排序，只移动值就好利用冒泡排序
    public static ListNode sortInList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode move = head;
        while (move.next != null) {
            ListNode temp = move.next;
            while (temp != null) {
                if (temp.val < move.val) {
                    int val = temp.val;
                    temp.val = move.val;
                    move.val = val;
                }
                temp = temp.next;
            }
            move = move.next;
        }
        return head;
    }
}
