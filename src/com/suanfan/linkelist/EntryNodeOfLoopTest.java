package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.HashSet;
import java.util.Set;

public class EntryNodeOfLoopTest {
    public ListNode EntryNodeOfLoop(ListNode pHead) {
        Set<ListNode> set = new HashSet<>();
        while (pHead != null){
            if (!set.add(pHead)){
                return pHead;
            }
            pHead = pHead.next;
        }
        return pHead;
    }
}
