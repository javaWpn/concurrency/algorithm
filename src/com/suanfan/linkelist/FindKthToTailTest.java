package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

public class FindKthToTailTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
//        final ListNode listNode3 = new ListNode(3);
//        final ListNode listNode4 = new ListNode(4);
//        final ListNode listNode5 = new ListNode(1);
//        final ListNode listNode6 = new ListNode(4);
//        final ListNode listNode7 = new ListNode(-1);
        listNode1.next = listNode2;
//        listNode2.next = listNode3;
//        listNode3.next = listNode4;
//        listNode4.next = listNode5;
//        listNode5.next = listNode6;
//        listNode6.next = listNode7;
        System.out.println(removeNthFromEnd(listNode1, 1));
    }

    public static ListNode FindKthToTail(ListNode pHead, int k) {
        if (k == 0) {
            return null;
        }
        ListNode fast = pHead;
        ListNode slow = pHead;
        for (int i = 0; i < k; i++) {
            if (fast != null) {
                fast = fast.next;
            } else {
                return null;
            }
        }
        while (fast != null) {
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }

    //快慢指针 移除倒数第k个链表
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode fast = head;
        ListNode slow = head;
        for (int i = 0; i < n; i++) {
            fast = fast.next;
        }
        if (fast == null) {
            return head.next;
        }
        while (fast.next != null) {
            slow = slow.next;
            fast = fast.next;
        }
        slow.next = slow.next.next;
        return head;
    }
}
