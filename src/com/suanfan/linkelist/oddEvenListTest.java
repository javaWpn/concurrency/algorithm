package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

public class oddEvenListTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(4);
        final ListNode listNode5 = new ListNode(5);
        final ListNode listNode6 = new ListNode(6);
        final ListNode listNode7 = new ListNode(7);
        final ListNode listNode8 = new ListNode(8);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        listNode7.next = listNode8;
        System.out.println(oddEvenList(listNode1));
    }

    public static ListNode oddEvenList(ListNode head) {
        if (head == null) return head;
        ListNode oddHead = head;
        ListNode evenHead = head.next;
        ListNode odd = oddHead;
        ListNode even = evenHead;
        while (even != null && even.next != null) {
            odd.next = even.next;
            odd = odd.next;
            even.next = odd.next;
            even = even.next;
        }
        odd.next = evenHead;
        return head;
    }
}
