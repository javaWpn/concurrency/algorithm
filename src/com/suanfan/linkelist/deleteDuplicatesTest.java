package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class deleteDuplicatesTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(5);
        final ListNode listNode5 = new ListNode(1);
        final ListNode listNode6 = new ListNode(4);
        final ListNode listNode7 = new ListNode(1);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        System.out.println(deleteDuplicates(listNode1));
    }

    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        Set<Integer> set = new TreeSet<>();
        Set<Integer> sets = new HashSet<>();
        while (head != null) {
            if (!set.add(head.val)){
                sets.add(head.val);
            }else {
                set.add(head.val);
            }
            head = head.next;
        }
        ListNode node = new ListNode(-1);
        ListNode cur = node;
        for (Integer val : set) {
            for (Integer vals : sets) {
                if (val != vals){
                    cur.next = new ListNode(val);
                    cur = cur.next;
                }
            }
        }
        return node.next;
    }

    public static ListNode deleteDuplicatess(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode cur = head;
        while (cur != null) {
            if (cur.next != null && cur.val == cur.next.val) {
                cur.next = cur.next.next;
            }
            cur = cur.next;
        }
        return head;
    }

}
