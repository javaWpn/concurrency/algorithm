package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.Stack;

/**
 * @author Wpn
 * @title: ReverseKGroup
 * @projectName 算法
 * @description: TODO
 * @date 2022/6/27 17:03
 */
public class ReverseKGroup {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(5);
        final ListNode listNode5 = new ListNode(1);
        final ListNode listNode6 = new ListNode(4);
        final ListNode listNode7 = new ListNode(-1);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        System.out.println(ReverseKGroup(listNode1, 2));
    }

    public static ListNode ReverseKGroup(ListNode head, int k) {
        System.out.println(head);
        if (head == null || head.next == null) {
            return head;
        }
        Stack<ListNode> stack = new Stack<>();
        int num = 0;
        ListNode pre = head;
        while (pre != null){
            pre = pre.next;
            num++;
        }
        if (num < k){
            return head;
        }
        ListNode rangeNode = null;
        ListNode next = null;
        while (head != null){
            next = head.next;
            head.next = null;
            stack.push(head);
            if (stack.size() == k){
                while (!stack.isEmpty()){
                    if (rangeNode == null){
                        rangeNode = stack.pop();
                        continue;
                    }
                    ListNode curs = rangeNode;
                    while (curs.next != null){
                        curs = curs.next;
                    }
                    curs.next = stack.pop();
                }
            }
            head = next;
        }
        ListNode node = null;
        while (!stack.isEmpty()){
            ListNode popNode = stack.pop();
            popNode.next = node;
            node = popNode;
        }
        ListNode curs = rangeNode;
        while (curs.next != null){
            curs = curs.next;
        }
        curs.next = node;
        return rangeNode;
    }
}
