package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

public class Merge {

    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(-1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(3);
        final ListNode listNode4 = new ListNode(2);
        final ListNode listNode5 = new ListNode(4);
        final ListNode listNode6 = new ListNode(5);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        System.out.println(Merge(listNode1, listNode4));
    }

    public static ListNode Merge(ListNode list1, ListNode list2) {
        if(list1==null){
            return list2;
        }
        else if(list2==null){
            return list1;
        }
        if(list2.val>list1.val){
            list1.next = Merge(list1.next,list2);
            return list1;
        }
        else{
            list2.next = Merge(list1,list2.next);
            return list2;
        }
    }
}
