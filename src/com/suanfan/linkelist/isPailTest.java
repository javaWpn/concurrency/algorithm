package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.Stack;

public class isPailTest {
    public static void main(String[] args) {
        final ListNode listNode1 = new ListNode(1);
        final ListNode listNode2 = new ListNode(2);
        final ListNode listNode3 = new ListNode(2);
        final ListNode listNode4 = new ListNode(3);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        System.out.println(isPail(listNode1));
    }
    public static boolean isPail (ListNode head) {
        if (head.next == null){
            return true;
        }
        ListNode pre = null;
        ListNode next = null;
        StringBuilder a = new StringBuilder();
        StringBuilder b = new StringBuilder();
        while (head != null){
            next = head.next;
            a.append(head.val);
            head.next = pre;
            pre = head;
            head = next;
        }
        while (pre != null){
            b.append(pre.val);
            pre = pre.next;
        }
        return a.toString().equals(b.toString());
    }
}
