package com.suanfan.linkelist.domain;

import java.util.Arrays;

public class CycleQueue {
    private int max;
    private int[] queue;
    private int front = 0;
    private int rear = 0;

    public CycleQueue(int max) {
        this.max = max;
        queue = new int[max];
    }

    public boolean isEmpty(){
        return front == rear;
    }

    public boolean isFull(){
        return front == (rear+1)%max;
    }

    public String getQueue(){
        return Arrays.toString(queue);
    }

    public void addNode(int value){
        if (isFull()){
            System.out.println("队列已满！");
            return;
        }
        queue[rear++] = value;
        rear = rear % max;
    }

    public int removeNode() {
        if(isEmpty()){
            throw new RuntimeException("队列为空，不能取出数据");
        }
        int value = queue[front];
        front++;
        front = front % max;
        return value;
    }

    public int size(){//计算数组中的有效数据个数
        return (rear + max - front) % max;
    }

    public static void main(String[] args) {
        CycleQueue cycleQueue = new CycleQueue(5);
        cycleQueue.addNode(1);
        cycleQueue.addNode(2);
        System.out.println(cycleQueue.removeNode());
        cycleQueue.addNode(3);
        System.out.println(cycleQueue.removeNode());
        cycleQueue.addNode(4);
        cycleQueue.addNode(5);
        System.out.println(cycleQueue.removeNode());
        cycleQueue.addNode(6);
        System.out.println(cycleQueue.getQueue());
        System.out.println(cycleQueue.size());
    }
}
