package com.suanfan.linkelist.domain;

import java.util.Arrays;

public class OrderQueue {
    private int max;
    private int[] queue;
    private int front = 0;
    private int rear = 0;

    public OrderQueue(int max) {
        this.max = max;
        queue = new int[max];
    }

    public boolean isEmpty(){
        return front == rear;
    }

    public boolean isFull(){
        return rear == max;
    }

    public String getQueue(){
        return Arrays.toString(queue);
    }

    public void addNode(int value){
        if (isFull()){
            System.out.println("队列已满！");
            return;
        }
        queue[rear++] = value;
    }

    public void removeNode(){
        if (isEmpty()){
            System.out.println("队列已空！");
            return;
        }
        int[] array = new int[queue.length];
        for (int i = 0; i < rear-1; i++) {
            array[i] = queue[i+1];
        }
        queue = array;
        front++;
    }

    public static void main(String[] args) {
        OrderQueue orderQueue = new OrderQueue(5);
        orderQueue.addNode(1);
        orderQueue.removeNode();
        orderQueue.addNode(2);
        orderQueue.addNode(3);
        orderQueue.removeNode();
        orderQueue.addNode(4);
        orderQueue.addNode(5);
        orderQueue.addNode(6);
        System.out.println(orderQueue.getQueue());
    }

}
