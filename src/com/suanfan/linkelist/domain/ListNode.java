package com.suanfan.linkelist.domain;

/**
 * @author Wpn
 * @title: ListNode
 * @projectName 单链表
 * @description: TODO
 * @date 2022/6/24 14:14
 */
public class ListNode {
    public int val;
    public ListNode next = null;

    public ListNode(int val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                ", next=" + next +
                '}';
    }
}
