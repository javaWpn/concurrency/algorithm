package com.suanfan.linkelist;

import com.suanfan.linkelist.domain.ListNode;

import java.util.HashSet;
import java.util.Set;

public class HasCycle {
    public boolean hasCycle1(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        while (head != null) {
            if (!set.add(head)) {
                return true;
            }
            head = head.next;
        }
        return false;
    }

    public boolean hasCycle2(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while (fast != null && fast.next != null) {
                slow = slow.next;
                fast = fast.next.next;
            if (fast == slow) {
                return true;
            }
        }
        return false;
    }
}
